<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('ordenes', 'OrdenController');

Route::post('/ticket', 'OrdenController@buscarTicket')->name('ticket');

Route::post('/revision/agregar', 'OrdenController@storeRevision')->name('revision.agregar');

Route::resource('revisions', 'RevisionController');