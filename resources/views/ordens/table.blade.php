<table class="table table-responsive" id="ordens-table">
    <thead>
        <tr>
            <th>Ticket</th>
            <th>Nombres</th>
            <th>Apellidos</th>
            <th>Estado</th>
            <th>Direccion</th>
            <th>Serial</th>
            <th colspan="3">Acciones</th>
        </tr>
    </thead>
    <tbody>
    @foreach($ordens as $orden)
        <tr>
            <td>{!! $orden->tiket !!}</td>
            <td>{!! $orden->nombres !!}</td>
            <td>{!! $orden->apellidos !!}</td>
            <td>
                @if ( optional($orden->revisiones)->isEmpty() )
                    <span class="label label-warning">Pendiente</span>
                @else
                    <span class="label label-success">Revisada</span>
                @endif
            </td>
            <td>{!! $orden->direccion !!}</td>
            <td>{!! $orden->serial !!}</td>
            <td>
                {!! Form::open(['route' => ['ordenes.destroy', $orden->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ordenes.show', [$orden->tiket]) !!}" class='btn btn-default btn-sm'><i class="glyphicon glyphicon-eye-open"></i> Ver</a>
                    <a href="{!! route('ordenes.edit', [$orden->id]) !!}" class='btn btn-default btn-sm'><i class="glyphicon glyphicon-edit"></i> Editar</a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-sm', 'onclick' => "return confirm('Estas seguro?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>