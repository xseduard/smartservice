
<input type="hidden" name="orden_id"  value="{{ $orden->id }}">

<!-- Pieza Afectada Field -->
<div class="form-group col-sm-6">
    <label for="pieza_afectada">Pieza Afectada:</label>
    <input type="text" name="pieza_afectada" class="form-control">
</div>

<!-- Pieza Remplazada Field -->
<div class="form-group col-sm-6">
    <label for="pieza_remplazada">Pieza Remplazada:</label>
    <input type="text" name="pieza_remplazada" class="form-control">
</div>

<!-- Observaciones Field -->
<div class="form-group col-sm-12 col-lg-12">
    <label for="observaciones">Observaciones:</label>
    <textarea name="observaciones" id="observaciones" class="form-control" rows="10"></textarea>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    <button class="btn btn-warning"> <i class="fa fa-heartbeat"></i> Agregar Revisión</button>     
</div>
