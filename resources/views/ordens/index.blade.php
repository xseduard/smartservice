@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Lista de Ordenes</h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-success">
            <div class="box-body">
                    @include('ordens.table')
            </div>
        </div>
    </div>
@endsection

