
<table class="table table-bordered table-striped">
    <tr>
        <th>Id</th>
        <th>Ticket</th>
        <th>Nombre</th>
        <th>Apelldos</th>
    </tr>
    <tr>
        <td>{!! $orden->id !!}</td>
        <td>{!! $orden->tiket !!}</td>
        <td>{!! $orden->nombres !!}</td>
        <td>{!! $orden->apellidos !!}</td>
    </tr>
    <tr>
        <th>Dirección</th>
        <th>Serial</th>
        <th>Fecha de registro</th>
        <th>Última actualización</th>
    </tr>
    <tr>
        <td>{!! $orden->direccion !!}</td>
        <td>{!! $orden->serial !!}</td>
        <td>{!! $orden->created_at !!}</td>
        <td>{!! $orden->updated_at->diffForHumans() !!}</td>
    </tr>
</table>


<!-- Observacion Field -->
<div class="form-group">
    {!! Form::label('observacion', 'Observaciones:') !!}
    <p>{!! $orden->observacion !!}</p>
</div>


<!-- user At Field -->
<div class="form-group">
    {!! Form::label('user', 'Usuario que registra:') !!}
    <p>{!! $orden->user->name !!}</p>
</div>


