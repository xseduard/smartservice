@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Orden / Ticket: {{ $orden->tiket }} 
             @if ( optional($orden->revisiones)->isEmpty() )
                <span class="label label-warning">Pendiente</span>
            @else
                <span class="label label-success">Revisada</span>
            @endif
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        @include('flash::message')
        <div class="box box-primary">            
            <div class="box-body">
                <div class="row" style="padding: 20px">
                    @include('ordens.show_fields')                    
                </div>
            </div>
        </div>
        @foreach ($orden->revisiones as $revision)
            <div class="box box-success">
                <div class="box-header">
                    <h3 class="box-title pull-left">
                        Revisión No:  
                        <b>{{ $loop->iteration }}</b>                   
                    </h3>
                    <h3 class="box-title pull-right">
                        {{ $revision->created_at->format('d-m-Y') }}
                        ({{ $revision->created_at->diffForHumans() }})
                    </h3>
                </div>
                <div class="box-body">
                    <div class="row" style="padding: 20px">
                        <table class="table table-bordered">
                            <tr>
                                <th>Id:</th>
                                <th>Pieza Afectada:</th>
                                <th>Pieza Remplazada:</th>
                            </tr>
                            <tr>
                                <td>{{ $revision->id }}</td>
                                <td>{!! $revision->pieza_afectada !!}</td>
                                <td>{!! $revision->pieza_remplazada !!}</td>
                            </tr>
                        </table>                               
                        <h4>Observaciones:</h4>
                        {{ $revision->observaciones }}
                        <h4>Técnico:</h4>
                        {{ $revision->user->name }}
                    </div>
                </div>
            </div>
        @endforeach             
        @if (auth()->user()->role == 'tecnico')
        <div class="box box-warning">
            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'revision.agregar']) !!}

                        @include('ordens.revision-fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        @endif
    </div>
@endsection
