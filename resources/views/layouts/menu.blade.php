@if (auth()->user()->role == 'secretary')
	<li class="{{ Request::is('ordenes/create') ? 'active' : '' }}">
	    <a href="{!! route('ordenes.create') !!}"><i class="fa fa-plus"></i><span> Registrar Orden</span></a>
	</li>

	<li class="{{ Request::is('ordenes') ? 'active' : '' }}">
	    <a href="{!! route('ordenes.index') !!}"><i class="fa fa-list"></i><span> Lista de Ordenes</span></a>
	</li>
@else
	<li class="{{ Request::is('revisions*') ? 'active' : '' }}">
	    <a href="{!! route('revisions.index') !!}"><i class="fa fa-list"></i> <span>Lista de Revisiones</span></a>
	</li>
@endif

