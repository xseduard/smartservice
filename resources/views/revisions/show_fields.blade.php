<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $revision->id !!}</p>
</div>

<!-- Orden Id Field -->
<div class="form-group">
    {!! Form::label('orden_id', 'Orden Id:') !!}
    <p>{!! $revision->orden_id !!}</p>
</div>

<!-- Pieza Afectada Field -->
<div class="form-group">
    {!! Form::label('pieza_afectada', 'Pieza Afectada:') !!}
    <p>{!! $revision->pieza_afectada !!}</p>
</div>

<!-- Pieza Remplazada Field -->
<div class="form-group">
    {!! Form::label('pieza_remplazada', 'Pieza Remplazada:') !!}
    <p>{!! $revision->pieza_remplazada !!}</p>
</div>

<!-- Observaciones Field -->
<div class="form-group">
    {!! Form::label('observaciones', 'Observaciones:') !!}
    <p>{!! $revision->observaciones !!}</p>
</div>

<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{!! $revision->user_id !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $revision->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $revision->updated_at !!}</p>
</div>

