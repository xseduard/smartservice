<table class="table table-responsive" id="revisions-table">
    <thead>
        <tr>
            <th>Orden/Ticket</th>
            <th>Pieza Afectada</th>
            <th>Pieza Remplazada</th>
            <th>Observaciones</th>
            <th>Técnico</th>
            <th>Fecha</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($revisions as $revision)
        <tr>
            <td>{!! $revision->orden->tiket !!}</td>
            <td>{!! $revision->pieza_afectada !!}</td>
            <td>{!! $revision->pieza_remplazada !!}</td>
            <td>{!! $revision->observaciones !!}</td>
            <td>{!! $revision->user->name !!}</td>
            <td>{!! $revision->created_at->format('d-m-Y') !!}</td>
            <td>
                {!! Form::open(['route' => ['revisions.destroy', $revision->id], 'method' => 'delete']) !!}
                <div class='btn-group'>                    
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i> Eliminar', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Esta seguro?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>