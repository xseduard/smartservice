<!-- Orden Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('orden_id', 'Orden Id:') !!}
    {!! Form::text('orden_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Pieza Afectada Field -->
<div class="form-group col-sm-6">
    {!! Form::label('pieza_afectada', 'Pieza Afectada:') !!}
    {!! Form::text('pieza_afectada', null, ['class' => 'form-control']) !!}
</div>

<!-- Pieza Remplazada Field -->
<div class="form-group col-sm-6">
    {!! Form::label('pieza_remplazada', 'Pieza Remplazada:') !!}
    {!! Form::text('pieza_remplazada', null, ['class' => 'form-control']) !!}
</div>

<!-- Observaciones Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('observaciones', 'Observaciones:') !!}
    {!! Form::textarea('observaciones', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('revisions.index') !!}" class="btn btn-default">Cancel</a>
</div>
