@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">

    	


		<div class="content-title" style="margin-top: 50px">
            <div class="title m-b-md">
                SmartService
            </div>
            <p>Aplicación para mantenimineto y restauración de dispositivos informaticos</p>  

            <div class="content">
	    		<div class="clearfix"></div>

	       	 	<div class="col-sm-offset-2 col-sm-8">
	       	 		@include('flash::message')

	       	 		@auth
	       	 			<form method="POST" action="/ticket" class="" role="search" >
			                  {{ csrf_field() }}
			                <div class="form-group has-success">
			                	<label for="ticket">Digite número de Ticket</label>
				                <div class="input-group input-group-lg">
				                  <input type="text" class="form-control" name="ticket" placeholder="Buscar ticket..." required>
				                 <span class="input-group-btn">
							        <button class="btn btn-success" type="submit"><i class="fa fa-search"></i></button>
							      </span>
				                </div>
			                </div>
			            </form>
	       	 		@endauth

	       	 	</div>
	    	</div>  

             

        </div>

    </div>
</div>
@endsection
