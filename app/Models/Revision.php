<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Revision
 * @package App\Models
 * @version November 12, 2017, 11:49 pm -05
 *
 * @property integer orden_id
 * @property string pieza_afectada
 * @property string pieza_remplazada
 * @property string observaciones
 * @property integer user_id
 */
class Revision extends Model
{
    use SoftDeletes;

    public $table = 'revisiones';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'orden_id',
        'pieza_afectada',
        'pieza_remplazada',
        'observaciones',
        'user_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'orden_id' => 'integer',
        'pieza_afectada' => 'string',
        'pieza_remplazada' => 'string',
        'observaciones' => 'string',
        'user_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'orden_id' => 'required',
        'pieza_afectada' => 'required',
        'observaciones' => 'required'
    ];


    public function user()
    {
        return $this->belongsTo('App\User');
    } 


    public function orden()
    {
        return $this->belongsTo('App\Models\Orden');
    } 

    
}
