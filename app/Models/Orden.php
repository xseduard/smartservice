<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Orden
 * @package App\Models
 * @version November 12, 2017, 9:25 pm -05
 *
 * @property string nombres
 * @property string apellidos
 * @property string direccion
 * @property string serial
 * @property string observacion
 * @property string tiket
 */
class Orden extends Model
{
    use SoftDeletes;

    public $table = 'ordenes';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'nombres',
        'apellidos',
        'direccion',
        'serial',
        'observacion',
        'tiket',
        'user_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'nombres' => 'string',
        'apellidos' => 'string',
        'direccion' => 'string',
        'serial' => 'string',
        'observacion' => 'string',
        'tiket' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nombres' => 'required',
        'apellidos' => 'required',
        'direccion' => 'required',
        'serial' => 'required'
    ];


    public function user()
    {
        return $this->belongsTo('App\User');
    } 

    public function revisiones()
    {
        return $this->hasMany('App\Models\Revision');
    }

    
}
