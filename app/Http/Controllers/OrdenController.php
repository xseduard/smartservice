<?php

namespace App\Http\Controllers;

use App\Http\Controllers\AppBaseController;
use App\Http\Requests\CreateOrdenRequest;
use App\Http\Requests\CreateRevisionRequest;
use App\Http\Requests\UpdateOrdenRequest;
use App\Models\Orden;
use App\Models\Revision;
use App\Repositories\OrdenRepository;
use Flash;
use Illuminate\Http\Request;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class OrdenController extends AppBaseController
{
    /** @var  OrdenRepository */
    private $ordenRepository;

    public function __construct(OrdenRepository $ordenRepo)
    {
        $this->middleware('auth');
        $this->ordenRepository = $ordenRepo;
    }

    /**
     * Display a listing of the Orden.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ordenRepository->pushCriteria(new RequestCriteria($request));
        $ordens = $this->ordenRepository->all();

        return view('ordens.index')
            ->with('ordens', $ordens);
    }

    /**
     * Show the form for creating a new Orden.
     *
     * @return Response
     */
    public function create()
    {
        return view('ordens.create');
    }

    /**
     * Store a newly created Orden in storage.
     *
     * @param CreateOrdenRequest $request
     *
     * @return Response
     */
    public function store(CreateOrdenRequest $request)
    {
        $input = $request->all();
        $input['tiket'] = rand();
        $input['user_id'] = auth()->id();

        $orden = $this->ordenRepository->create($input);

        Flash::success('Orden Registrada con el ticket Número: <b>'. $orden->tiket .' </b>');

        return redirect(route('ordenes.index'));
    }

    /**
     * Display the specified Orden.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($ticket)
    {
        $orden = Orden::where('tiket', $ticket)->first();

        if (empty($orden)) {
            Flash::error('Ticket número '. $ticket .' No encontrado');

            return redirect(route('home'));
        }

        return view('ordens.show')->with('orden', $orden);
    }

     public function buscarTicket(Request $request)
    {
        $orden = Orden::where('tiket', $request->ticket)->first();

        if (empty($orden)) {
            Flash::error('Ticket número <b>'. $request->ticket .'</b> No encontrado');

            return redirect(route('home'));
        }

        return view('ordens.show')->with('orden', $orden);
    }


    /**
     * Show the form for editing the specified Orden.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $orden = $this->ordenRepository->findWithoutFail($id);

        if (empty($orden)) {
            Flash::error('Orden no encontrada');

            return redirect(route('ordenes.index'));
        }

        return view('ordens.edit')->with('orden', $orden);
    }

    /**
     * Update the specified Orden in storage.
     *
     * @param  int              $id
     * @param UpdateOrdenRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOrdenRequest $request)
    {
        $orden = $this->ordenRepository->findWithoutFail($id);

        if (empty($orden)) {
            Flash::error('Orden no encontrada');

            return redirect(route('ordenes.index'));
        }

        $input = $request->all();
        $input['user_id'] = auth()->id();

        $orden = $this->ordenRepository->update($input, $id);

        Flash::success('Orden actualizada.');

        return redirect(route('ordenes.index'));
    }

    /**
     * Remove the specified Orden from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $orden = $this->ordenRepository->findWithoutFail($id);

        if (empty($orden)) {
            Flash::error('Orden no encontrada');

            return redirect(route('ordenes.index'));
        }

        $this->ordenRepository->delete($id);

        Flash::success('Orden Eliminada.');

        return redirect(route('ordenes.index'));
    }

    public function storeRevision(CreateRevisionRequest $request)
    {
        $input = $request->all();
        $input['user_id'] = auth()->id();

        $revision = Revision::create($input);

        Flash::success('Revisión registrada correctamente.');

        return redirect(route('ordenes.show', [$revision->orden->tiket]));
    }
}
