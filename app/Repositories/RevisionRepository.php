<?php

namespace App\Repositories;

use App\Models\Revision;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class RevisionRepository
 * @package App\Repositories
 * @version November 12, 2017, 11:49 pm -05
 *
 * @method Revision findWithoutFail($id, $columns = ['*'])
 * @method Revision find($id, $columns = ['*'])
 * @method Revision first($columns = ['*'])
*/
class RevisionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'pieza_afectada',
        'pieza_remplazada',
        'observaciones',
        'user_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Revision::class;
    }
}
