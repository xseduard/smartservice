<?php

namespace App\Repositories;

use App\Models\Orden;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class OrdenRepository
 * @package App\Repositories
 * @version November 12, 2017, 9:25 pm -05
 *
 * @method Orden findWithoutFail($id, $columns = ['*'])
 * @method Orden find($id, $columns = ['*'])
 * @method Orden first($columns = ['*'])
*/
class OrdenRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
     
             'tiket' => 'like',
             'nombres' => 'like',
             'apellidos' => 'like',
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Orden::class;
    }
}
